<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeActivityTypeSingle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('activity_type_id');
        });

        Schema::table('activities', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('activity_type_id')->references('id')->on('activity_types');
        });

        Schema::dropIfExists('activity_types_activities');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropForeign('user_id');
            $table->dropForeign('activity_type_id');
            $table->dropColumn('user_id');
            $table->dropColumn('activity_type_id');
        });

        Schema::create('activity_types_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('activity_id');
            $table->unsignedInteger('type_id');

            $table->foreign('activity_id')->references('id')->on('activities');
            $table->foreign('type_id')->references('id')->on('activity_types');
        });
    }
}
