<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameActivityTypeToCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('activity_types', 'categories');

        Schema::table('activities', function (Blueprint $table) {
            $table->renameColumn('activity_type_id', 'category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('categories', 'activity_types');

        Schema::table('activities', function (Blueprint $table) {
            $table->renameColumn('category_id', 'activity_type_id');
        });
    }
}
