@extends('layouts.app')

@section('content')
    <div class="row">
        @foreach($categories as $category)
            <div class="col-4">
                <div class="card">
                    <div class="card-header justify-content-between d-flex">
                        <div></div>
                        <div>{{$category->name}}</div>
                        <div><a class="js-remove-category" href="{{ route('categories_delete', ['id' => $category->id]) }}">x</a></div>
                    </div>
                    <div class="card-body"></div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
