<div class="sidebar-header">
    <a href="{{ url('/') }}"><h3>{{ config('app.name', 'Tracker') }}</h3></a>
</div>

<ul class="list-unstyled components">
    <p>Keep calm and stay productive</p>
    <li{{ \Request::route()->getName() == 'index' ? ' class=active' : '' }}>
        <a href="{{ route('index') }}">Home</a>
    </li>
    <li{{ \Request::route()->getName() == 'categories' ? ' class=active' : '' }}>
        <a href="{{ route('categories') }}">Categories</a>
    </li>
    {{--<li class="active">--}}
        {{--<a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Expanding--}}
            {{--example</a>--}}
        {{--<ul class="collapse list-unstyled" id="homeSubmenu">--}}
            {{--<li>--}}
                {{--<a href="#">Expanded item 1</a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="#">Expanded item 2</a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="#">Expanded item 3</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</li>--}}
</ul>

{{--<ul class="list-unstyled CTAs">--}}
    {{--<li>--}}
        {{--<a href="#" class="download">Example</a>--}}
    {{--</li>--}}
    {{--<li>--}}
        {{--<a href="#" class="article">Example</a>--}}
    {{--</li>--}}
{{--</ul>--}}