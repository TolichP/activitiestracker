
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

// document.addEventListener("DOMContentLoaded", function() {
//
//     document.querySelector("#sidebarCollapse").addEventListener("click", function () {
//         document.querySelector("#sidebar").classList.toggle('active');
//         this.classList.toggle('active');
//     });
//
//     var removeLinks = document.querySelectorAll(".js-remove-category");
//     for(var i=0; i < removeLinks.length; i++){
//         removeLinks[i].addEventListener("click", function (ev) {
//             ev.preventDefault();
//
//             console.log(ev.target);
//         });
//     }
//
// });

//TODO make prettier
$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $(this).toggleClass('active');
    });

    $('.js-remove-category').on('click', function (ev, el) {
        ev.preventDefault();

        $.ajax({
            url: $(this).attr('href'),
            method: 'DELETE',
            success: function(data, textStatus, jqXHR){
                if (jqXHR.status !== 204) {
                    return;
                }
                $(this).closest('.card').addClass('alert-danger');

            }.bind(this),
            fail: function (response) {}
        });
    });
});