<?php

use Illuminate\Http\Request;
use App\Category;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('categories', 'Api\CategoriesController@index')->name('categories_index');
Route::get('categories/{category}', 'CategoriesController@show')->name('categories_show');
Route::post('categories', 'CategoriesController@store')->name('categories_store');
Route::put('categories/{category}', 'CategoriesController@update')->name('categories_update');
Route::delete('categories/{category}', 'Api\CategoriesController@delete')->name('categories_delete');
