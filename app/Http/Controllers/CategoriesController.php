<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

//TODO move logic to service layer
class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('categories', [
            'categories' => Category::where('user_id', \Auth::id())
                                    ->where('is_active', 1)
                                    ->get()
        ]);
    }

    /**
     * Removes category with provided id by setting is_active to false
     *
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        $answer = Category::where('id', $id)->update('is_active', 0);
        return \Response::$answer;
    }
}
