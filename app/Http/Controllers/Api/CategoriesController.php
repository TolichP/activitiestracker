<?php

namespace App\Http\Controllers\Api;

use App\Services\CategoriesService;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

//TODO move logic to service layer
class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

//    public function index()
//    {
//        return Category::all();
//    }
//
//    public function show(Category $category)
//    {
//        return $category;
//    }
//
//    public function store(Request $request)
//    {
//        $category = Category::create($request->all());
//
//        return response()->json($category, 201);
//    }
//
//    public function update(Request $request, Category $category)
//    {
//        $category->update($request->all());
//
//        return response()->json($category, 200);
//    }

    public function delete(Request $request, Category $category, CategoriesService $categoriesService)
    {
        $isSuccess = $categoriesService->delete($category);
        return response()->json(null, $isSuccess ? 204 : 401);
    }
}