<?php

namespace App;


/**
 * App\ActivityType
 *
 * @mixin \Eloquent
 * @property string $icon_url
 * @property int $is_active
 * @property int $user_id
 * @property int $id
 * @property string $name
 * @property string $color
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Activity[] $activities
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereColorUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereIconUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUserId($value)
 */
class Category extends Model
{
    public function activities() {
        return $this->hasMany('App\Activity');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
