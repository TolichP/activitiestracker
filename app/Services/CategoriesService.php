<?php

namespace App\Services;


use App\Category;

class CategoriesService {

    public function delete(Category $category) {
        if ($category->user_id !== \Auth::getUser()->id) {
            return false;
        }

        return $category->delete();
    }
}