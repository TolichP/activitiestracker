<?php

namespace App;


/**
 * App\Activity
 *
 * @property int $id
 * @property int $duration
 * @property string $created_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $types
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereId($value)
 * @mixin \Eloquent
 * @property int $activity_type_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereActivityTypeId($value)
 * @property-read \App\Category $type
 */
class Activity extends Model
{

    public function type() {
        return $this->belongsTo('App\Category');
    }

    public static function boot() {
        parent::boot();

        static::creating(function (Model $model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
}
